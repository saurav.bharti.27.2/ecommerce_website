
import React, { useState } from 'react'
import Header from '../header/Header';
import './Checkout.css'
import { useGlobalContext } from '../../context/GlobalContext';

export default function Checkout() {

    const {name, setName} = useGlobalContext()
    const {email, setEmail} = useGlobalContext()
    const {phone, setPhone} = useGlobalContext()
    const {bankAccount, setBankAccount} = useGlobalContext()

    const onChange= (event)=>{

        const targetted_event= event.target.id;
        if(targetted_event.includes('name')){
            setName(event.target.value)
        }else if(targetted_event.includes('email')){
            setEmail(event.target.value)
        }else if(targetted_event.includes('phone')){
            setPhone(event.target.value)
        }else{
            setBankAccount(event.target.value)
        }
    }

    const {totalPrice, setTotalPrice} = useGlobalContext()
  return (
    <div>
        <Header />
        <form>
            <label>Name</label>
            <input type="text" id="name_input" placeholder='Enter your name' value={name} onChange={onChange}  required/>

            <label>Email</label>
            <input type="email" id="email_input" placeholder='Enter your email' value={email} onChange={onChange} required />

            <label>Phone No</label>
            <input type="number" id="phone_input" placeholder='Enter your phone' value={phone} onChange={onChange} required />

            <label>Bank Account No.</label>
            <input type="number" id="bank_account" placeholder='Bank Account No.' value={bankAccount}  onChange={onChange} required/>

            <div id="make_payment">
                <button type='submit' id="make_payment_btn">Pay, $ {totalPrice.toFixed(2)}</button>
            </div>
        </form>

    </div>
  )
}
