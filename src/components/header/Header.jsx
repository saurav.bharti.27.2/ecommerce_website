import React from 'react'
import { Link } from 'react-router-dom'
import './Header.css'

export default function Header() {
  return (
    <>
        <div className='navbar'>
            <Link to='/'>Home</Link>
            <Link to='/cart'>Cart</Link>
            <Link to='/checkout'>Checkout</Link>
        </div>
    </>
  )
}
