import React from 'react'
import './Table.css'
import { useGlobalContext } from '../../context/GlobalContext'
export default function Table({shopData}) {

    const {cartItems, setCartItems} = useGlobalContext()
    let {totalPrice, setTotalPrice} = useGlobalContext()

    let shopItems = Array.of(shopData)

    const addToCart =(e)=> {
        e.preventDefault()
        
        const idItem = (e.target.id).slice(9)
        const itemSelected = shopData[idItem]

        if(!cartItems[idItem]){
            cartItems[idItem] = 0
        }
        cartItems[idItem] += 1;

        totalPrice += (itemSelected.price)

        setTotalPrice(totalPrice)
        setCartItems(cartItems)
        
    }

    const subtractTheCount = (event) => {
        const subId = event.target.id.slice(4)
        
        let duplicate_array = {}
    
        Object.entries(cartItems).map((item)=>{
          
          if(Number(item[0]) == Number(subId)){
    
            totalPrice-=(shopItems[0][Number(item[0])].price);
    
            if(Number(cartItems[item[0]]) == 1){
              
            }else{
              duplicate_array[item[0]] = (item[1] - 1);
            }
          }else{
            duplicate_array[item[0]] = item[1]
          }
        })
        setTotalPrice(totalPrice)
        setCartItems(duplicate_array)
      }

  return (
    <div className='table'>
        {
            shopData.map((item,index) => {
                return (
                    <div className='items'>
                        <div className='item_property'>
                            <span className='title_item'>{item.title}</span>
                            <div className='price'>
                                <span id="item_price">$ {item.price}</span>
                                {
                                   !cartItems[index] ? 
                                   <button className='btn item' id={`btn-item-${index}`} onClick={addToCart}>  Add to Cart</button>
                                   : 
                                   <div className='change_the_cart_items'>
                                        <button className="sub_btn" id={`sub_${index}`} onClick={subtractTheCount}> - </button>
                                        <button className="add_btn" id={`btn_item_${index}`} onClick={addToCart}> + </button>
                                        <span>{cartItems[index]}</span>
                                    </div>

                                }

                            </div>
                        </div>
                        <div className='item_image'>
                            <img src={item.image} alt={item.title} />


                        </div>
                    </div>
                )
            })
        }
    </div>
  )
}
