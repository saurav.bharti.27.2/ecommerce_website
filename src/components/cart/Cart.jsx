import React, { useEffect } from 'react'
import Header from '../header/Header'
import { useGlobalContext } from '../../context/GlobalContext'
import { useNavigate } from 'react-router-dom'
import './Cart.css'

export default function Cart() {

  const {cartItems, setCartItems, shopData, setShopData}  = useGlobalContext()
  let {totalPrice, setTotalPrice} = useGlobalContext()
  const navigate = useNavigate()

  let shopItems = Array.of(shopData)

  const removeFromCart = (event) => {
    const removeId = event.target.id.slice(4)

    const newCartItems = {}

    Object.entries(cartItems).map((item)=>{

      if(item[0]!== removeId){
        newCartItems[item[0]] = item[1] 
      }else{
        totalPrice -= (Number(item[1]) * shopItems[0][Number(item[0])].price)
        setTotalPrice(totalPrice)
      }
    })

    setCartItems(newCartItems)
  }

  const addTheCount = (event) => { 
      const addId = event.target.id.slice(4)

      let duplicate_array = {}

      Object.entries(cartItems).map((item)=>{
        
        if(Number(item[0]) == Number(addId)){
          duplicate_array[item[0]] = (item[1] + 1);
          
          totalPrice += (shopItems[0][Number(item[0])].price)
        }else{
          duplicate_array[item[0]] = item[1]
        }
      })

      setTotalPrice(totalPrice)
      setCartItems(duplicate_array)

  }
  const subtractTheCount = (event) => {
    const subId = event.target.id.slice(4)
    
    let duplicate_array = {}

    Object.entries(cartItems).map((item)=>{
      
      if(Number(item[0]) == Number(subId)){

        totalPrice-=(shopItems[0][Number(item[0])].price);

        if(Number(cartItems[item[0]]) == 1){
          
        }else{
          duplicate_array[item[0]] = (item[1] - 1);
        }
      }else{
        duplicate_array[item[0]] = item[1]
      }
    })
    setTotalPrice(totalPrice)
    setCartItems(duplicate_array)
  }


  const checkoutBtn = ()=> {
    navigate('/checkout')
  }

  return (
    <div>
        <Header />
        <div className='cart-title'>
          <div className='product'>Product</div>
          <div className='quantity'>Quantity</div>
          <div className='price_card'>Price</div>
          <div className='remove'>Remove</div>
        </div>
        <hr className='horizontal_rule'/>
        <div className='cart-items'>
          {
            Object.entries(cartItems).map((item) => {
              return (
                <div className='item-cart' id={`item-cart${item[0]}`}>
                  {

                    <div className='each_item'>
                      <span className='product_title'>{shopItems[0][Number(item[0])].title} </span>

                      <span className='quantity' >
                        <span className='subtractCounter' onClick={subtractTheCount}><i class="fa-solid fa-arrow-down" id={`sub-${item[0]}`}  ></i></span>
                        <span>{item[1]}</span>
                        <span className="addCounter" onClick={addTheCount}><i class="fa-solid fa-arrow-up" id={`add-${item[0]}`} ></i></span>
                      </span>

                      <span className='price_item'> $ {shopItems[0][Number(item[0])].price }</span>

                      <div className='remove_item'>
                        <button id={`btn-${item[0]}`} className='remove_btn' onClick={removeFromCart}> X </button>

                      </div>
                    </div>
                  }
                </div>
              )
            })
          }
        </div>

        <hr className='horizontal_rule'/>
        <div className='total_price'>
          <h3 className='total'>Total</h3>
          <h2 className='total_cost'>$ {totalPrice.toFixed(2)}</h2>
        </div>

        <div className='payment'>
          <button className='payment_btn' onClick={checkoutBtn}>Checkout</button>
        </div>
    </div>
  )
}
