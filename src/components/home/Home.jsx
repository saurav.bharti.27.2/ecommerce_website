import React, { useEffect, useState } from 'react'
import Header from '../header/Header'
import Table from '../table/Table'
import { useGlobalContext } from '../../context/GlobalContext'

export default function Home() {

    const {shopData, setShopData} = useGlobalContext()

    const fetch_shop_data=()=> {
        fetch('https://fakestoreapi.com/products')
            .then(res=>res.json())
            .then( (data)=>{
                setShopData(data)
            })
    }


    useEffect(()=> {
        fetch_shop_data();
    }, [])

  return (
    <div>
        <Header />
        {
            shopData && <Table shopData={shopData} /> 
        }
    </div>
  )
}
