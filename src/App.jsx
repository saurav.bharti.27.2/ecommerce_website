
import './App.css'
import {Routes, Route} from 'react-router-dom'
import Home from './components/home/Home'
import Cart from './components/cart/Cart'
import Checkout from './components/checkout/Checkout'
import { Navigate } from 'react-router-dom'

function App() {
  
  return (
    <Routes>
      <Route index path='/' element={<Home />} />
      <Route path='/cart' element={<Cart />} /> 
      <Route path='/checkout' element={<Checkout /> } />
      <Route path='*' element={<Navigate to="/" />} />
    </Routes>
  )
}

export default App
