import React from "react";
import { useContext, useState } from "react";

const GlobalContext = React.createContext()

export function useGlobalContext(){
    return useContext(GlobalContext)
}

export function GlobalProvider({children}){

    const [cartItems, setCartItems] = useState({})
    const [shopData, setShopData] = useState([])
    const [totalPrice, setTotalPrice] = useState(0)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [bankAccount, setBankAccount] = useState('')


    const value= {
        cartItems,
        setCartItems,
        shopData,
        setShopData,
        totalPrice,
        setTotalPrice,
        name, 
        setName,
        email, 
        setEmail,
        phone,
        setPhone,
        bankAccount,
        setBankAccount
    }


    return <GlobalContext.Provider value={value}>{children}</GlobalContext.Provider>
}